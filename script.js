// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Екранування - це спосіб використання спеціальних символів як звичайних, шляхом додавання через символ екранування.
//
// 2.	Які способи оголошення функцій ви знаєте?
  // Існує три способа оголошення функції в JS:
  // 1.Function declaration – оголошується за допомогою ключового слова function, вона створюється до початку виконання JS коду, 
  //тому її можна викликати до того місця де вона була оголошена.
  // 2.Function expression – функціональний вираз. Оголошується за допомогою ключового слова function, але вона не має свого власного 
  //імені і записується в змінну. Можна викликати лише після її оголошення в коді.
  // 3.Named Function expression - оголошується за допомогою ключового слова function, а також має своє ім’я
  //( правда її не можна викликати по цьому імені). Можна викликати лише після її оголошення в коді.
//
// 3. Що таке hoisting, як він працює для змінних та функцій?
//Це механізм, в якому змінні та оголошення функцій піднімаються вверх своєї області видимості перед тим як код буде виконаний. 
//Сам код при цьому лишається незмінним. 


function createNewUser(firstName, lastName, birthday) {
    firstName = prompt("What is your first name?").trim();
    lastName = prompt("What is your last name?").trim();
    birthday = prompt("Write your birthday in format: dd.mm.yyyy").trim();
    
    return {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin: function () {
        let fullName = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        return fullName
        },
        getAge: function(){
        let myBirthday = this.birthday.split('.');
        myBirthday = new Date(myBirthday[2], myBirthday[1] - 1, myBirthday[0]);
        let now = new Date();
        let userBirthday = new Date(myBirthday);  
        let age = Math.floor((now - userBirthday) / (1000 * 3600 * 24 * 365.25));
        return age;
      },
       getPassword: function(){
        let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(-4, 4);
        return password;
      }
    }
    

    let newUser = createNewUser();
      
    console.log(newUser);
    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
}
